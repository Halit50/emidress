<?php

namespace App\Form;

use App\Entity\Appointement;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RendezVousType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('date', DateTimeType::class, [
                'disabled' => true,
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'html5' => false,
                'attr' => [
                    'class' => 'text-center'
                ]
            ])
            ->add('startTime', TextType::class, [
                'disabled' => true,
                'label' => 'Heure de début',
                'attr' => [
                    'class' => 'text-center'
                ]
            ])
            ->add('endTime', TextType::class, [
                'disabled' => true,
                'label' => 'Heure de fin',
                'attr' => [
                    'class' => 'text-center'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Confirmer le rdv',
                'attr' => [
                    'class' => 'btn-info btn-sm'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Appointement::class,
        ]);
    }
}
