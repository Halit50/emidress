<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ChangePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstname', TextType::class, [
                'disabled' => true,
                'label' => 'Mon prénom',
                'attr' => [
                    'class' => "w-50 m-auto"
                ]
            ])
            ->add('lastname', TextType::class, [
                'disabled' => true,
                'label' => 'Mon nom',
                'attr' => [
                    'class' => "w-50 m-auto"
                ]
            ])
            ->add('email', EmailType::class, [
                'disabled' => true,
                'label' => 'Mon Adresse Email',
                'attr' => [
                    'class' => "w-50 m-auto"
                ]
            ])
            ->add('old_password', PasswordType::class, [
                'label' => 'Mon mot de passe actuel',
                'mapped' => false,
                'attr' => [
                    'class' => "w-50 m-auto",
                    'placeholder' => 'Veuillez saisir votre mot de passe actuel'
                ]
            ])
            ->add(
                'new_password',
                RepeatedType::class,
                [
                    'type' => PasswordType::class,
                    'mapped' => false,
                    'invalid_message' => 'Le mot de passe et la confirmation doivent être identiques',
                    'required' => true,
                    'first_options' => [
                        'label' => 'Mon nouveau mot de passe',
                        'attr' => [
                            'placeholder' => 'Merci de saisir votre nouveau mot de passe',
                            'class' => 'w-50 m-auto'
                        ]
                    ],
                    'second_options' => [
                        'label' => 'Confirmez votre nouveau mot de passe',
                        'attr' => [
                            'placeholder' => 'Merci de confirmer votre nouveau mot de passe',
                            'class' => 'w-50 m-auto'
                        ]
                    ]
                ]
            )
            ->add('submit', SubmitType::class, [
                'label' => "Mettre à jour",
                'attr' => [
                    'class' => 'btn btn-info m-auto'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
