<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Validator\Constraints\Length;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstname', TextType::class, [
                'label' => 'Prénom *',
                'attr' => [
                    'class' => "w-50 m-auto",
                    'placeholder' => 'Veuillez saisir votre prénom'
                ]
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Nom *',
                'attr' => [
                    'class' => "w-50 m-auto",
                    'placeholder' => 'Veuillez saisir votre nom'
                ]
            ])
            ->add('phone', TelType::class, [
                'label' => 'Numéro de Téléphone *',
                'constraints' => new Length(10),
                'attr' => [
                    'class' => "w-50 m-auto",
                    'placeholder' => 'Veuillez saisir votre numéro de téléphone'
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => 'Adresse Email *',
                'attr' => [
                    'class' => "w-50 m-auto",
                    'placeholder' => 'Veuillez saisir votre adresse mail'
                ]
            ])
            ->add(
                'password',
                RepeatedType::class,
                [
                    'type' => PasswordType::class,
                    'invalid_message' => 'Le mot de passe et la confirmation doivent être identiques',
                    'required' => true,
                    'first_options' => [
                        'label' => 'Mot de passe *',
                        'attr' => [
                            'placeholder' => 'Merci de saisir votre mot de passe',
                            'class' => 'w-50 m-auto'
                        ]
                    ],
                    'second_options' => [
                        'label' => 'Confirmez votre mot de passe *',
                        'attr' => [
                            'placeholder' => 'Confirmez votre mot de passe',
                            'class' => 'w-50 m-auto'
                        ]
                    ]
                ]
            )
            ->add('submit', SubmitType::class, [
                'label' => "S'inscrire",
                'attr' => [
                    'class' => 'btn btn-info m-auto'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
