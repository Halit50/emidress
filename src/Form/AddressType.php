<?php

namespace App\Form;

use App\Entity\Address;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Veuillez nommer votre adresse (exemple: domicile, lieu de travail etc)',
                'attr' =>[
                    'placeholder' => "Veuillez saisir un nom d'adresse",
                    'class' => 'w-50 m-auto'
                ]
            ])
            ->add('street', TextType::class, [
                'label' => 'Numéro et Nom de rue',
                'attr' => [
                    'placeholder' => 'Veuillez saisir votre numéro et votre nom de rue',
                    'class' => 'w-50 m-auto'
                ]
            ])
            ->add('postal', TextType::class, [
                'label' => 'Code Postal',
                'attr' => [
                    'placeholder' => 'Veuillez saisir votre code Postal',
                    'class' => 'w-50 m-auto'
                ]
            ])
            ->add('city', TextType::class, [
                'label' => 'Ville',
                'attr' => [
                    'placeholder' => 'Veuillez saisir votre ville',
                    'class' => 'w-50 m-auto'
                ]
            ])
            ->add('country', CountryType::class, [
                'label' => 'Pays',
                'attr' => [
                    'class' => 'w-50 m-auto',
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Valider',
                'attr' => [
                    'class' => 'btn btn-info btn-sm d-block m-auto'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
        ]);
    }
}
