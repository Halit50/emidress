<?php

namespace App\Classe;

use App\Entity\Category; 

// Création d'une class qui représente notre objet recherche effectué par l'utilisateur
class Search {
   
    /**
     * @var string
     */
    public $string= '';

    /**
     * @return string
     */
    public function getString(): string
    {
        return $this->string;
    }

    /**
     * @param string $string
     */
    public function setString(string $string): void
    {
        $this->string = $string;
    }

    /**
     * @var Category[]
     */
    public $categories = [];
}
