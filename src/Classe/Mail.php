<?php

namespace App\Classe;

use Mailjet\Client;
use Mailjet\Resources;

class Mail {

    private $api_public_key = 'f35115093da52959347e51f5c8e42985';
    private $api_private_key= '3285dcc5a520bccc69d3fbba7bfc89a9';

    public function send($userEmail, $userName, $subject, $title, $emailContent){

        $mj = new Client($this->api_public_key, $this->api_private_key, true,['version' => 'v3.1']);
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => "halit.cinici@gmail.com",
                        'Name' => "Emidress"
                    ],
                    'To' => [
                        [
                            'Email' => $userEmail,
                            'Name' => $userName,
                        ]
                    ],
                    'TemplateID' => 4221025,
                    'TemplateLanguage' => true,
                    'Subject' => $subject,
                    'Variables' => [
                        'title' => $title,
                        'email_content' => $emailContent
                    ]
                ]
            ]
        ];
        $response = $mj->post(Resources::$Email, ['body' => $body]);
        $response->success();
    }
}