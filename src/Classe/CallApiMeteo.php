<?php

namespace App\Classe;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class CallApiMeteo {

    private $client;
    private $apiKey = '2ab0c116ad40f97de3be741d417a0af4719caa519f13ebf5e5db319db259282b';


    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    public function getApi($codeInsee): array
    {
        $response = $this->client->request(
            'GET',
            'https://api.meteo-concept.com/api/forecast/daily?token='.$this->getApiKey().'&insee='.$codeInsee
        );
        return $response->toArray();
    }


}