<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
// ajouter le use Faker\Factory pour pouvoir utiliser Faker
use Faker\Factory;
use App\Entity\Product;
use App\Entity\Category;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // Utilisation de Faker
        $faker = Factory::create('fr_FR');
        for ($k = 0; $k < 5; $k++) {
            $categories = new Category();
            $categories->setName($faker->words(1, true));
            $manager->persist($categories);
            for ($i = 0; $i < 10; $i++) {
                $products = new Product();
                $products->setName($faker->words(3, true));
                $products->setSlug($faker->slug());
                $products->setImage($faker->imageUrl(360, 360, 'animals', true, 'dogs', true, 'jpg'));
                $products->setPrix($faker->randomFloat(2,100,1000));
                $products->setSubtitle($faker->words(5, true));
                $products->setDescription($faker->text(50));
                $products->setCategory($categories);
    
                $manager->persist($products);
            }
        }
        $manager->flush();
    }
}
