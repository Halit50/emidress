<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Classe\Mail;
use App\Entity\Order;
use App\Entity\OrderDetails;
use App\Form\OrderType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager){
        $this->entityManager = $entityManager;
    }
    /**
     * @Route("/commande", name="app_order")
     */
    public function index(Cart $cart, Request $request): Response
    {
        if (!$this->getUser()->getAddresses()->getValues()){
            return $this->redirectToRoute('app_address_add');
        }

        $form = $this->createForm(OrderType::class,null,[
            'user' => $this->getUser()
        ]);

        return $this->render('order/index.html.twig',[
            'form' => $form->createView(),
            'cart' => $cart->getFull()
        ]);
    }

    /**
     * @Route("/commande/recapitulatif", name="app_order_recap", methods={"POST"})
     */
    public function add(Cart $cart, Request $request): Response
    {
        $form = $this->createForm(OrderType::class,null,[
            'user' => $this->getUser()
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $date = new \DateTime();
            $carriers = $form->get('carriers')->getData();
            $delivery = $form->get('addresses')->getData();
            $delivery_content = $delivery->getName();
            $delivery_content .= '<br>'.$delivery->getStreet();
            $delivery_content .= '<br>'.$delivery->getPostal().' '.$delivery->getCity();
            $delivery_content .= '<br>'.$delivery->getCountry();

            // Enregistrer ma commannde sur mon entity Order
            $order = new Order();
            $reference = $date->format('dmY').'-'.uniqid();
            $order->setReference($reference);
            $order->setUser($this->getUser());
            $order->setCreatedAt($date);
            $order->setCarrier($carriers);
            $order->setDelivery($delivery_content);
            $order->setState(0);

            $this->entityManager->persist($order);

            //Enregister sur mon entity Order_details

            foreach ($cart->getFull() as $product) {
                $orderDetails = new OrderDetails();
                $orderDetails->setMyOrder($order);
                $orderDetails->setProduct($product['product']);
                $orderDetails->setQuantity($product['quantity']);
                $orderDetails->setPrice($product['product']->getPrix());
                $orderDetails->setTotal($product['product']->getPrix() * $product['quantity']);
                $this->entityManager->persist($orderDetails);
            }

            $this->entityManager->flush();

            return $this->render('order/recap.html.twig',[
                'cart' => $cart->getFull(),
                'delivery' => $delivery_content,
                'carrier' => $carriers,
                'reference' => $order->getReference()
            ]);
        }
        return $this->redirectToRoute('app_cart');

    }

    /**
     * @Route("/commande/succes/{stripeSessionId}", name="payment_succes")
     */
    public function orderSuccess($stripeSessionId, Cart $cart): Response
    {
        $order= $this->entityManager->getRepository(Order::class)->findOneByStripeSessionId($stripeSessionId);

        // sécurité si la commande n'est pas trouvé pour x raison ou si l'utilisateur qui effectue la commande est bien celui qui est actuellement connecté, rediriger vers l'accueil
        if (!$order || $order->getUser() != $this->getUser()) {
            return $this->redirectToRoute('home');
        }

        // Si le statut getState est à 0 on lui indique de passer en 1 pour indiqué que la commande a bien été payée
        if ($order->getState() == 0){
            // On vide le panier de l'utlisateur
            $cart->remove();

            $order->setState(1);
            $this->entityManager->flush();

            $mail = new Mail();
            $content = "Félicitation votre commande est bien validée, pour suivre votre commande, retourner sur votre compte Emidress";
            $mail->send($order->getUser()->getEmail(), $order->getUser()->getFirstname(), 'Commande Validée','Bravo, Votre commande a bien été validée', $content);
        }


        return $this->render('order/succes.html.twig', [
            'order' => $order
        ]);
    }

    /**
     * @Route("/commande/erreur/{stripeSessionId}", name="payment_error")
     */
    public function orderError($stripeSessionId): Response
    {
        $order= $this->entityManager->getRepository(Order::class)->findOneByStripeSessionId($stripeSessionId);

        // sécurité si la commande n'est pas trouvé pour x raison ou si l'utilisateur qui effectue la commande est bien celui qui est actuellement connecté, rediriger vers l'accueil
        if (!$order || $order->getUser() != $this->getUser()) {
            return $this->redirectToRoute('home');
        }

        // Envoyer un mail à notre utilisateur pour lui indiquer l'échec de paiement

        return $this->render('order/error.html.twig', [
            'order' => $order
        ]);
    }
}
