<?php

namespace App\Controller;

use App\Classe\CallApiMeteo;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager){
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="home")
     */
    public function index(CallApiMeteo $callApiMeteo): Response
    {
        $dreuxInsee = 28134;
        $parisInsee = 75056;
        $chartresInsee = 28085;
        $lyonInsee = 69123;
        $marseilleInsee = 13055;

        $datasDreux = $callApiMeteo->getApi($dreuxInsee);
        $datasChartres = $callApiMeteo->getApi($chartresInsee);
        $datasParis = $callApiMeteo->getApi($parisInsee);
        $datasLyon = $callApiMeteo->getApi($lyonInsee);
        $datasMarseille = $callApiMeteo->getApi($marseilleInsee);

        $productsBest = $this->entityManager->getRepository(Product::class)->findByIsBest(1);
        $productsGreat = $this->entityManager->getRepository(Product::class)->findByIsGreat(1);

        return $this->render('home/index.html.twig', [
            'products' => $productsBest,
            'productsGreat' => $productsGreat,
            'datasDreux' => $datasDreux,
            'datasChartres' => $datasChartres,
            'datasParis' => $datasParis,
            'datasLyon' => $datasLyon,
            'datasMarseille' => $datasMarseille
        ]);
    }
}
