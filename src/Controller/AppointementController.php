<?php

namespace App\Controller;

use App\Classe\Mail;
use App\Entity\Appointement;
use App\Form\RendezVousType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PropertyAccess\PropertyAccess;

class AppointementController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/prendre-rendez-vous", name="app_appointement")
     */
    public function index(): Response
    {

        $appointements = $this->entityManager->getRepository(Appointement::class)->findByDate();

        return $this->render('appointement/index.html.twig', [
            'appointements' => $appointements,
        ]);
    }

    /**
     * @Route("/prendre-rendez-vous/{id}", name="app_appointement_confirm")
     */
    public function confirm(Request $request, $id): Response
    {
        $appointement = $this->entityManager->getRepository(Appointement::class)->findOneById($id);
        $form = $this->createForm(RendezVousType::class,$appointement);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $user = $this->getUser();
            $appointement->setUser($user);
            $this->entityManager->flush();
            $mail = new Mail();
            $content = "Félicitation votre rendez-vous est bien confirmée, vous pouvez nous retrouver à l'adresse suivante: 56 rue des Lillas 75000 Paris";
            $mail->send($this->getUser()->getEmail(), $this->getUser()->getFirstname(), 'Rendez-vous confirmé','Bravo, Votre Rendez-vous est pris', $content);
            $this->addFlash('messageConfirm', "Votre rendez-vous a bien été pris, l'adresse vous sera envoyé par mail ");
            return $this->redirectToRoute('app_appointement');
        }

        return $this->render('appointement/confirm.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
