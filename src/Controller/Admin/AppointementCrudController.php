<?php

namespace App\Controller\Admin;

use App\Entity\Appointement;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TimeField;

class AppointementCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Appointement::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            DateField::new('date'),
            TextField::new('startTime', 'Heure de début'),
            TextField::new('endTime', 'Heure de fin'),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
       return $crud
           ->setEntityLabelInSingular('Rendez-vous')
           ->setEntityLabelInPlural('Rendez-vous');
    }
}
