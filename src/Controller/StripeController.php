<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Entity\Order;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Stripe\Checkout\Session;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StripeController extends AbstractController
{
    /**
     * @Route("/commande/create-session/{reference}", name="app_stripe")
     */
    public function index(EntityManagerInterface $entityManager,Cart $cart, $reference): Response
    {
        $product_for_stripe = [];
        $YOUR_DOMAIN = 'https://localhost:8000/';

        $order = $entityManager->getRepository(Order::class)->findOneByReference($reference);

        // Ajout d'une securité au cas pour X raison la commande n'a pas été bien faite
        if (!$order){
            new JsonResponse(['error' => 'order']);
        }

        foreach ($order->getOrderDetails()->getValues() as $product){
            // Ajout des produits de notre panier dans stripe
            $product_for_stripe[] = [
                'price_data' => [
                    'currency' => 'eur',
                    'product_data' => [
                        'name' => $product->getProduct()->getName()
                    ],
                    'unit_amount' => round($product->getPrice())

                ],
                'quantity' => $product->getQuantity()];
        }

        // Ajout du prix du transporteur
        $product_for_stripe[] = [
            'price_data' => [
                'currency' => 'eur',
                'product_data' => [
                    'name' => $order->getCarrier()->getName()
                ],
                'unit_amount' => round($order->getCarrier()->getPrice())

            ],
            'quantity' => 1];


        Stripe::setApiKey('sk_test_51LiLoMDGYOFHKYepnY3xMBT5vwMJWH2XR3ntN9GpHXYtapN29AvQVty21GPUx0qVa2J6MWFr69ke3Yq1p3MJL1yV00kCU59YvE');

        // le $checkout_session create va crée un id de session pour stripe afin de récupérer les données de ma commande
        $checkout_session = Session::create([
            'customer_email' => $this->getUser()->getEmail(),
            'success_url' => $YOUR_DOMAIN . 'commande/succes/{CHECKOUT_SESSION_ID}',
            'cancel_url' => $YOUR_DOMAIN . 'commande/erreur/{CHECKOUT_SESSION_ID}',
            'line_items' => [
                $product_for_stripe
            ],
            'mode' => 'payment'
        ]);

        // On ajoute le $checkout_session->id dans notre order puis flush le paiement afin de récupérer notre commande sur la page de succes ou error
        $order->setStripeSessionId($checkout_session->id);
        $entityManager->flush();

        $response = new JsonResponse(['id' => $checkout_session->id]);
        return $response;
    }
}
