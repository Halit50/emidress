<?php

namespace App\Controller;

use App\Classe\Mail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Form\RegisterType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterController extends AbstractController
{
    /**
     * @Route("/inscription", name="app_register")
     */
    public function index(Request $request,UserPasswordEncoderInterface $encoder): Response
    {
        $notif = null;

        // Création d'un nouvel utilisateur
        $user = new User();

        // Création du formulaire
        $form = $this->createForm(RegisterType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $password = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $user->setRoles(['ROLE_USER']);
            $user = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $mail = new Mail();
            $content = "Félicitation votre inscription est terminée, vous pouvez dès à présent profiter de tous nos produits";
            $mail->send($user->getEmail(), $user->getFirstname(), 'Bienvenue sur notre site','Inscription finalisée', $content);

            $notif = "Bravo, votre inscription est maintenant terminée. Dès à présent, vous pouvez vous connecter et profiter de nos meilleurs offres";

            return $this->redirectToRoute('app_login');
        }

        return $this->render('register/index.html.twig', [
            'form' => $form->createView(),
            'notif' => $notif
        ]);
    }
}
