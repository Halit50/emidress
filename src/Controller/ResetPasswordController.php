<?php

namespace App\Controller;

use App\Classe\Mail;
use App\Form\ResetPasswordType;
use DateTime;
use App\Entity\ResetPassword;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ResetPasswordController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/mot-de-passe-oublie", name="app_reset_password")
     */
    public function index(Request $request): Response
    {
        // Si l'user est connecté, on le redirige vers la page d'accueil
        if($this->getUser()){
            return $this->redirectToRoute('home');
        }

        // Si on a un mail d'indiqué lors de l'envoi de la requête
        if ($request->get('email'))
        {
            //On récupère l'user correspondant à l'adresse mail saisie (en base)
            $user = $this->entityManager->getRepository(User::class)->findOneByEmail($request->get('email'));

            // si l'user existe
            if ($user)
            {
                // Etape 1 : Enregistrer en base la demande de reset_password avec user,token,createdAt
                $reset_password = new ResetPassword();
                $reset_password->setUser($user);
                $reset_password->setToken(uniqid());
                $reset_password->setCreatedAt(new DateTime());
                $this->entityManager->persist($reset_password);
                $this->entityManager->flush();

                // Etape 2: envoyer un email a nos utilisateurs avec un lien lui permettant de mettre à jour son mot de passe

                $url = $this->generateUrl('update_password',[
                    'token' => $reset_password->getToken()
                ]);

                $content = "Bonjour".$user->getFirstname()."<br> Vous avez demandé à réinitialiser votre mot de passe sur notre site Emidress<br><br>";
                $content .= "Merci de bien vouloir cliquer sur le lien suivant pour <a href='".$url."'>mettre à jour votre mot de passe</a>";

                $mail = new Mail();
                $mail->send($user->getEmail(),$user->getFirstname().' '.$user->getLastname(),'Réinitialisation du mot de passe','Réinitialiser votre mot de passe sur Emidress', $content);

                $this->addFlash('notice', 'Vous aller recevoir un mail afin de réinitialiser votre mot de passe');
            } else {
                $this->addFlash('notice', "Cette adresse email est inconnue");
            }
        }

        return $this->render('reset_password/index.html.twig');
    }

    /**
     * @Route("/modifier-mon-mot-de-passe/{token}", name="update_password")
     */
    public function update(Request $request, $token, UserPasswordEncoderInterface $encoder)
    {
        /// On récupère en base la demande de reset effectué avec le token
        $reset_password = $this->entityManager->getRepository(ResetPassword::class)->findOneByToken($token);

        // Si il n'y a pas de demande correspondante en base, on redirige sur la page du reset password
        if (!$reset_password){
            return $this->redirectToRoute('app_reset_password');
        }

        // vérifier si le createdAt = now -3h qui nous permet de garder la validité de la modif pendant 3h
        $now = new DateTime();
        if ($now >$reset_password->getCreatedAt()->modify('+ 3 hour')){
            $this->addFlash('notice','Votre demande de mot de passe a expiré. Merci de le renouveller');
            return $this->redirectToRoute('reset_password');
        }

        //Rendre une vue avec mot de passe et confirmez votre mot de passe
        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $new_pwd = $form->get('new_password')->getData();

            // Encodage des mots de passe
            $password = $encoder->encodePassword($reset_password->getUser(), $new_pwd);
            $reset_password->getUser()->setPassword($password);


            // Flush en base de données
            $this->entityManager->flush();

            // Redirection de l'utilisateur vers la page de connexion
            $this->addFlash('notice', 'Votre mot de passe a bien été mis à jour');
            return $this->redirectToRoute('app_login');
        }

        return $this->render ('reset_password/update.html.twig',[
            'form'=>$form->createView()
        ]);






    }
}
