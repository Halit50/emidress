<?php

namespace App\Controller;

use App\Classe\Search;
use App\Form\SearchType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Product;

class ProductController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/nos-produits", name="app_product")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        $search = new Search();
        $form = $this->createForm(SearchType::class, $search, array( 'csrf_protection' => false,));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            // On execute la recherche effectué par l'utilisateur gràace à la fonction que nous avons crées dans le repo de Product
            $data = $this->entityManager->getRepository(Product::class)->findWithSearch($search);
            $products = $paginator->paginate(
                $data,
                $request->query->getInt('page', 1),
                9
            );
        } else {
            $data = $this->entityManager->getRepository(Product::class)->findAll();
            $products = $paginator->paginate(
                $data,
                $request->query->getInt('page', 1),
                9
            );
            if (isset($_GET['search'])){
                $searchNavbar = new Search();
                $searchNavbar->setString($_GET['search']);
                $data = $this->entityManager->getRepository(Product::class)->findWithSearch($searchNavbar);
                $products = $paginator->paginate(
                    $data,
                    $request->query->getInt('page', 1),
                    9
                );
            }
        }

        return $this->render('product/index.html.twig', [
            'products' => $products,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/nos-produits/{slug}", name="single_product")
     */
    public function show($slug): Response
    {
        $product = $this->entityManager->getRepository(Product::class)->findOneBySlug($slug);

        if (!$product){
            return $this->redirectToRoute('app_product');
        }

        return $this->render('product/show.html.twig', [
            'product' => $product
        ]);
    }

    /**
     * @Route("/robes-mariage", name="app_product_mariage")
     */
    public function marriedDress(): Response
    {
        $products = $this->entityManager->getRepository(Product::class)->findByMarried();

        return $this->render('product/married_dress.html.twig', [
            'products' => $products,
        ]);
    }

    /**
     * @Route("/robes-soiree", name="app_product_night")
     */
    public function nightDress(): Response
    {
        $products = $this->entityManager->getRepository(Product::class)->findByNight();

        return $this->render('product/night_dress.html.twig', [
            'products' => $products,
        ]);
    }
}
