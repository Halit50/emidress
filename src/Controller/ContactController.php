<?php

namespace App\Controller;

use App\Form\ContactType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function sendEmail(Request $request, MailerInterface $mailer): Response
    {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $contact = $form->getData();

            // Ici nous envoyons le mail
            $message = (new Email())
                // On attribue l'expéditeur
                ->from($contact['email'])
                //->setReplyTo($mail)

                // On attribue le destinataire
                ->to('halit.cinici@gmail.com')

                ->subject($contact['objet'])

                ->text($contact['message']);

                //on crée le message avec la vue Twig
                //->html($this->renderForm('contact/email.html.twig'), 'text/html');


            // On envoie le message
            $mailer->send($message);

            $this->addFlash('message', 'Votre message a bien été envoyé, nous vous recontacterons dans les plus bref délais');
            return $this->redirectToRoute('home');
        }

        return $this->render('contact/index.html.twig', [
            'contactform' => $form->createView()
        ]);
    }
}
