<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Entity\Address;
use App\Entity\Appointement;
use App\Entity\Order;
use App\Form\AddressType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ChangePasswordType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AccountController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager){
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/mon-compte", name="app_account")
     */
    public function index(): Response
    {
        return $this->render('account/index.html.twig');
    }

    /**
     * @Route("/mon-compte/modification-mot-de-passe", name="app_account_change_password")
     */
    public function changePassword(Request $request,UserPasswordEncoderInterface $encoder): Response
    {
        $notif = null;
        
        // Récupération de l'utilisateur connecté
        $user = $this->getUser();
        $form = $this->createForm(ChangePasswordType::class, $user);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) { 
            
        }

        if ($form->isSubmitted() && $form->isValid()){
            // On récupère l'ancien mdp
            $old_pwd = $form->get('old_password')->getData();
            // On vérifie que l'ancien mdp saisie correspond bien au mdp encodé en bdd
            if ($encoder->isPasswordValid($user, $old_pwd)){
                // On récupère le nouveau mdp saisie
                $new_mdp = $form->get('new_password')->getData();
                // On encode le nouveau mdp
                $password = $encoder->encodePassword($user, $new_mdp);
                $user->setPassword($password);
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $notif = 'Votre mot de passe a bien été mis à jour';
            } else {
                $notif = "Votre mot de passe actuel n'est pas le bon";
            }
        }

        return $this->render('account/changepassword.html.twig', [
            'form' => $form->createView(),
            'notif' => $notif
        ]);
    }

    /**
     * @Route("/mon-compte/mes-adresses", name="app_address")
     */
    public function address(): Response
    {
        return $this->render('account/address.html.twig');
    }

    /**
     * @Route("/mon-compte/mes-adresses/ajouter-une-adresse", name="app_address_add")
     */
    public function addAddress(Cart $cart, Request $request): Response
    {
        $address = new Address();
        $form = $this->createForm(AddressType::class, $address);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $address->setUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($address);
            $em->flush();
            if ($cart->get()){
                return $this->redirectToRoute('app_order');
            } else {
                return $this->redirectToRoute('app_address');
            }
        }

        return $this->render('account/addressadd.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/mon-compte/mes-adresses/modifier-mon-adresse/{id}", name="app_address_edit")
     */
    public function editAddress(Request $request, $id): Response
    {
        $address = $this->entityManager->getRepository(Address::class)->findOneById($id);

        if (!$address || $address->getUser() != $this->getUser()){
            return $this->redirectToRoute('app_address');
        }
        $form = $this->createForm(AddressType::class, $address);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirectToRoute('app_address');
        }

        return $this->render('account/address_edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/mon-compte/mes-adresses/supprimer-mon-adresse/{id}", name="app_address_delete")
     */
    public function deleteAddress($id): Response
    {
        $address = $this->entityManager->getRepository(Address::class)->findOneById($id);

        if ($address && $address->getUser() == $this->getUser()){
            $this->entityManager->remove($address);
            $this->entityManager->flush();

        }
        return $this->redirectToRoute('app_address');
    }

    /**
     * @Route("/mon-compte/mes-commandes", name="app_account_order")
     */
    public function accountOrder(): Response
    {
        $user = $this->getUser();
        $orders = $this->entityManager->getRepository(Order::class)->findSuccesOrder($user);

        return $this->render('account/order.html.twig',[
            'orders' => $orders
        ]);
    }

    /**
     * @Route("/mon-compte/mes-commandes/{reference}", name="app_account_order_show")
     */
    public function accountOrderShow($reference): Response
    {
        $order = $this->entityManager->getRepository(Order::class)->findOneByReference($reference);

        if (!$order || $order->getUser() != $this->getUser()){
            return $this->redirectToRoute('app_account_order');
        }

        return $this->render('account/order_show.html.twig', [
            'order' => $order
        ]);
    }

    /**
     * @Route("/mon-compte/mes-rendez-vous", name="app_account_appointement")
     */
    public function accountAppointement(): Response
    {
        $appointements = $this->entityManager->getRepository(Appointement::class)->findByUser($this->getUser());

        return $this->render('account/appointement.html.twig', [
            'appointements' => $appointements
        ]);
    }

    /**
     * @Route("/mon-compte/mes-rendez-vous/{id}/delete", name="app_account_appointement_delete")
     */
    public function accountAppointementDelete($id): Response
    {
        $appointement = $this->entityManager->getRepository(Appointement::class)->findOneById($id);
        $appointement->setUser(null);
        $this->entityManager->flush();

        return $this->redirectToRoute('app_account_appointement');
    }

}
