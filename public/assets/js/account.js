function sizeCard() {
    let screenWidth = (window.innerWidth)
    let accountCard = document.getElementsByClassName('card-count');

    if (screenWidth < 1000){
        for (let i = 0; i < accountCard.length; i++){
            accountCard[i].style.minHeight = '15vh';
        }
    } else {
        for (let i = 0; i < accountCard.length; i++){
            accountCard[i].style.minHeight = '30vh';
        }
    }
    window.addEventListener('resize', e => {
        let screenResize = this.innerWidth;
        if (screenResize < 1000){
            for (let i = 0; i < accountCard.length; i++){
                accountCard[i].style.minHeight = '15vh';
            }
        } else {
            for (let i = 0; i < accountCard.length; i++){
                accountCard[i].style.minHeight = '30vh';
            }
        }
    });
}

setTimeout(function(){
    sizeCard();
}, 100);


