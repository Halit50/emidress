// Confirmation suppression adresse

function deleteAddresse() {
    const bouttonAnnulerRdv = document.getElementById('supprAdresse');
    bouttonAnnulerRdv.addEventListener('click', e => {
        let rep = confirm('Etes-vous sûr de vouloir supprimer cette adresse?');
        if(rep) {
            alert('Votre adresse a bien été supprimé')
        } else {
            e.preventDefault();
        }
    })
}

deleteAddresse();

