function carouselDnone () {
    let screenWidth = (window.innerWidth)
    let carousel = document.getElementById('carouselExampleControls');

    if (screenWidth < 1000){
        carousel.classList.add('d-none');
    } else {
        carousel.classList.remove('d-none');
    }
    window.addEventListener('resize', e => {
        let screenResize = this.innerWidth;
        if (screenResize < 1000){
            carousel.classList.add('d-none');
        } else {
            carousel.classList.remove('d-none');
        }
    });

}

carouselDnone();