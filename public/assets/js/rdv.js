// Confirmation annuler rendez-vous

function deleteAppointement(){
    const bouttonAnnulerRdv = document.getElementById('annulerRdv');
    bouttonAnnulerRdv.addEventListener('click', e => {
        let rep = confirm('Etes-vous sûr de vouloir annuler votre rendez-vous?');
        if(rep) {
            alert('Votre rendez-vous a bien été annulé')
        } else {
            e.preventDefault();
        }
    })
}

deleteAppointement();

