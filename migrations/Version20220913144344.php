<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220913144344 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE size (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, disponibility TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_order DROP FOREIGN KEY FK_5475E8C48C0FA77');
        $this->addSql('DROP TABLE product_order');
        $this->addSql('ALTER TABLE order_details ADD product VARCHAR(255) NOT NULL, ADD quantity INT NOT NULL, ADD price DOUBLE PRECISION NOT NULL, CHANGE my_order_id my_order_id INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE product_order (id INT AUTO_INCREMENT NOT NULL, order_details_id INT NOT NULL, price DOUBLE PRECISION NOT NULL, quantity INT NOT NULL, INDEX IDX_5475E8C48C0FA77 (order_details_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE product_order ADD CONSTRAINT FK_5475E8C48C0FA77 FOREIGN KEY (order_details_id) REFERENCES order_details (id)');
        $this->addSql('DROP TABLE size');
        $this->addSql('ALTER TABLE order_details DROP product, DROP quantity, DROP price, CHANGE my_order_id my_order_id INT NOT NULL');
    }
}
